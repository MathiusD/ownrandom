import unittest
from own.own_random.functions import Rule_1, Rule_2

class Test_Own_Random(unittest.TestCase):

    def test_rule_1(self):
        self.assertEqual(True, Rule_1(1))
        self.assertEqual(False, Rule_1(0))
        self.assertEqual(True, Rule_1(99))

    def test_rule_2(self):
        self.assertEqual(False, Rule_2(0))